// VARIABLES
var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0,
    isFirefox = typeof InstallTrigger !== 'undefined',
    isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0,
    isChrome = !!window.chrome && !isOpera,
    isIE = /*@cc_on!@*/false || !!document.documentMode,
    isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) );

// OBJECTS

// Add all your "on window resize" functions to one place. So that there is only one listener at any one time
// Give each listener a unique name to help clean up duplicates and to allow easier targeting
// onWindowResize.add("customName", "functionName.init()");
// onWindowResize.add("customName", "functionName.init()", 800);
// onWindowResize.add("customName", ["functionName1.init()", functionName2.init()], [200,1000]);
var onWindowResize = {
  names: [],
  functions: [],
  breakpoints: [],
  devMode: false, // Site will perform slower if set to true.
  init:function() {
    $(window).on('resize', function(){ onWindowResize.resize(); });
    setTimeout(function() { onWindowResize.resize(); }, 100);

    // Remove Duplicate functions by name (and acoompaning function & breakpoints).
    var names = onWindowResize.names;
    var functions = onWindowResize.functions;
    var breakpoints = onWindowResize.breakpoints;
    onWindowResize.names = [];
    onWindowResize.functions = [];
    onWindowResize.breakpoints = [];
    for (var i = names.length; i--; ) {
      var val = names[i];
      if ($.inArray(val, onWindowResize.names) === -1) {
        onWindowResize.names.unshift(val);
        onWindowResize.functions.unshift(functions[i]);
        onWindowResize.breakpoints.unshift(breakpoints[i]);
      }
    }

    // Output logs in the console
    if (onWindowResize.devMode) {
      setTimeout(function() {
        var functionsCount = 0;

        console.log(onWindowResize.functions);
        console.log(onWindowResize.breakpoints);

        $.each(onWindowResize.functions, function(i) {
          if (isArray(onWindowResize.functions[i])) {
            $.each(onWindowResize.functions[i], function() { functionsCount ++ });
          } else {
            functionsCount ++;
          }
        });

        console.log("When this page loaded, "+functionsCount+" 'onWindowResize' function" + (functionsCount > 1 ? "s were " : " was ") + "called");
      }, 1000);
    }
  },
  resize: function() {
    $.each(onWindowResize.functions, function(i, val) {
      // If there is a breakpoint condition applied, perform a limits check before calling the function.
      // However, if the breakpoint condition was never set (false), then call the function no matter what.
      if (isArray(onWindowResize.functions[i])) {
        $.each(onWindowResize.functions[i], function(l, vals) {
          if (checkBreakPointLimits(onWindowResize.breakpoints[i]) || onWindowResize.breakpoints[i] == false) {
            eval(vals);
            if (onWindowResize.devMode) { logFunctions(i, onWindowResize.functions[i]); }
          }
        });
      } else {
        if (checkBreakPointLimits(onWindowResize.breakpoints[i]) || onWindowResize.breakpoints[i] == false) {
          eval(val);
          if (onWindowResize.devMode) { logFunctions(i, val); }
        }
      }
    });
    function logFunctions(i, func) {
      var functionLog = "";
          breakpointLog = onWindowResize.breakpoints[i],
          nameLog = onWindowResize.names[i];
          grammer2 = (isArray(breakpointLog) && breakpointLog.length != 1) ? 's "'+breakpointLog[0]+' to '+breakpointLog[1]+'"' : ' "'+breakpointLog+'" and above';
          grammer1 = !breakpointLog ? ' at any width' : ' between breakpoint' + grammer2;

      if (isArray(func)) {
        $.each(func, function(q, value) {
          functionLog = functionLog + "|| Function " + (q+1) + ": " + value + "\n";
        });
      } else {
        functionLog = "|| Function: " + func + "\n";
      }
      console.log('%cOn window resize, run %c"'+nameLog+'"%c'+grammer1, 'color: green; font-weight: bold;', 'color: red; font-weight: bold;', 'color: green; font-weight: bold;');
      console.log('%c'+functionLog, "color:black");
    }
  },
  add: function(name, func, breaks) {
    if ( breaks == false || breaks == "" || breaks == null ) { breaks = false; }
    onWindowResize.names.push(name);
    onWindowResize.functions.push(func);
    onWindowResize.breakpoints.push(breaks);
  },
  remove: function(name) {
    var item = onWindowResize.names.indexOf(name);
    setTimeout(function() {
      if(item !== -1) {
        onWindowResize.names.splice(item, 1);
        onWindowResize.functions.splice(item, 1);
        onWindowResize.breakpoints.splice(item, 1);
      }
    }, 200);
  }
}

// onOrientationChange.add("functionName.init()");
// TODO: Update this function to be more similar to the onWindowResize function
var onOrientationChange = {
  functions: [],
  orientations: [],
  init:function() {
    window.addEventListener("orientationchange", function() { onOrientationChange.change() }, false);
    setTimeout(function() { onOrientationChange.change() }, 100);
  },
  change: function() {
    //console.log(window.orientation);

    $.each(onOrientationChange.functions, function(i, val) {
      // If there is a breakpoint condition applied, perform a limits check before calling the function.
      // However, if the breakpoint condition was never set (false), then call the function no matter what.
      if (checkOrientation() == onOrientationChange.orientations[i] || onOrientationChange.orientations[i] == 'both') {
        eval(val);
        //console.log('Function: "'+val+'');
      }
    });
  },
  add: function(func, orient) {
    // Functions
    if (isArray(func)) {
      $.each(func, function(i, val) {
        onOrientationChange.functions.push(val);
      });
    } else {
      onOrientationChange.functions.push(func);
    }
    orient = !orient ? "both" : orient;
    onOrientationChange.orientations.push(orient);

  },
  remove: function(func) {
    // Remove all instances of a string/function from the array. Duplicate functions is a no-no.
    onOrientationChange.functions = $.grep(onOrientationChange.functions, function(value) { return value != func; });
  }
}

function checkOrientation() {
  if (window.orientation == -90 || window.orientation == 90) { return 'landscape'; }
  else if (window.orientation == 0 || window.orientation == 180) { return'portrait'; }
}

// Check if object/variable is an Array;
var isArray = (function () {
  if (Array.isArray) { return Array.isArray; }
  var objectToStringFn = Object.prototype.toString;
  var arrayToStringResult = objectToStringFn.call([]);
  return function (subject) {
    return objectToStringFn.call(subject) === arrayToStringResult;
  };
}());

var internetExplorer = {
  init : function() {
    // User "placeholder" plugin to fix Place Holders for all IE browsers
    $('input, textarea').placeholder();
    // Add some IE specific classes to the content container
    $('body').addClass("ie ie"+internetExplorer.version());
  },
  version : function() {
    if ( isIE ) {
      var ua = window.navigator.userAgent;
      var msie = ua.indexOf("MSIE ");
      if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
      } else {
        return false;
      }
    } else { return 99; }
  }
};

// FUNCTIONS

// Always keep array of elements the same height. You can limit at what breakpoints the resize listener works
// Usage Example 1: makeSameHeight([".first",  ".second", ".third"], [600, 1200]);
// Usage Example 2: makeSameHeight(".boxes");
// Usage Example 2: makeSameHeight(".boxes", 200); This will only work between when the window width is 200px or greater.
var makeSameHeightCounter = 0;
function makeSameHeight(arr, breakpoints, height) {
  makeSameHeightCounter ++;
  // Check if height parameter exists, and set default if not.
  var height = !height ? "min-height" : height;
  // Turn the array into a string
  var objs = arr.toString();
  // If the breakpoints parameter doesn't exist, set to false. Otherwise set it as an array. False = no break points will be used, and will resize at all widths
  if ( breakpoints == false || breakpoints == "" || breakpoints == null ) {
    var breaks = false;
  } else {
    // Small tweak to the syntax for arrays
    var breaks = isArray(breakpoints) ? "[" + breakpoints + "]" : breakpoints;
  }
  // Create unique name for each resizer, as duplicates will be deleted
  onWindowResize.add("Make Same Height "+makeSameHeightCounter, "resizeHeights('" + objs + "', " + breaks + ", '" + height + "')", breakpoints);
}

function resizeHeights(objs, breakpoints, height) {
  if (checkBreakPointLimits(breakpoints)) {
    // Reset height
    $(objs).css(height, "");
    // Set height
    $(objs).css(height, highest(objs));
  } else {
    // Reset height
    $(objs).css(height, "");
  }
}

function highest(objs) {
  if ($.type( new String(objs) ) === "string") { objs = $(objs) }
  var highest = 0;
  objs.each(function(i, ele) {
    highest = ($(ele).innerHeight() > highest) ? $(ele).innerHeight() : highest;
  });
  return highest;
}

// Make a selection of items change their hight according to your grid presets
// ELEMENTS: Give a single path to the items you want to manipulate
// BREAKPOINTS: Starting with the heightest breakpoint first, add each breakpoint in the array
// COLUMNS: List how items there are per row, in relation the the given breakpoints. Below example 3 colums within 850px width
// sameHeightGrid(".list article", [850, 550], [3, 2]);
function sameHeightGrid(items, breakpoints, columns, reset) {
  var total = $(items).length,
      reset = reset == false ? false : true;
      allElements = [];

  if (total) {
    // Add class to each element/items
    $(items).each(function(i) { $(this).addClass("height-item-"+(i+1)); });

    $.each(breakpoints, function(i, val) {
      elements = [];
      breaks = [];
      cols = columns[i];
      rows = Math.floor(total/cols)+total%cols; // Get total amount of columns, whilst including to add any rows for remainder items
      remainder = total % cols;

      if (i == 0) {
        breaks.push(breakpoints[0]);
      } else {
        breaks.push(breakpoints[i], (breakpoints[i-1])-1);
      }

      //console.log("%cRows: "+rows +  " (With "+remainder+" item"+(remainder > 1 || remainder == 0 ? "s" : "")+" in the last row) - Columns: "+cols, "font-weight:bold; color:red;");

      for ( var l = 0; l < total; l++ ) {
        elements.push(".height-item-" + (l+1));
        allElements.push(".height-item-" + (l+1));
        if ((l % cols) == (cols-1)) {
          // Once the loop has pushed the right amount of elements into the array for each row, call the makeSameheight function then reset the array
          //console.log(elements, breaks);
          makeSameHeight(elements, breaks);
          elements = [];
        } else if (l == total-1 && elements.length >= 1) {
          // If there is any remainder items, and there is no need to set the same heightif there is only one element. Then...
          //console.log(elements, breaks);
          makeSameHeight(elements, breaks);
        }
      };
    });

    // Allow the min-height to be removed if window is not within any breaking points (esentially reverting back to the original ccs markup)
    if (reset) {onWindowResize.add("sameHeightGrid", "$('"+allElements.toString()+"').css('min-height','')", [0,(breakpoints[breakpoints.length-1]-1)]); }
  }
}

// Load in Disqus
var disqus = {
  craft: function() {
    var disqus = '#disqus_thread', // This name can't be changed.
        disqus_shortname = $(disqus).attr('data-shortname'),
        disqus_identifier = $(disqus).attr('data-identifier'),
        disqus_script = document.createElement('script');
          disqus_script.type = 'text/javascript',
          disqus_script.async = true,
          disqus_script.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';

    $("body").append(disqus_script);
  }
}

// Add a notification bar if the user loses internet connection.
var connection = {
  init: function() {
    $( "body" ).attr( {"ononline": "connection.update()", "onoffline": "connection.update()" } );
  },
  status: function() {
    return navigator.onLine;
  },
  update: function() {
    if (navigator.onLine) {
      // Online
      $('body').removeClass('offline');
      $(".noticebar-offline").remove();
    } else {
      // Offline
      $('body').addClass('offline').prepend('<div class="noticebars"><div class="noticebar noticebar-offline"><div class="noticebar-message"><h6>Connection Lost</h6><p>You are currently disconnected from the internet. Please reconnect before you continue</p></div></div></div>');
    }
  }
};

var removeClass = {
  startWith : function(obj, classes) {
    $(obj).attr('class', function(i, c){ return c.replace('/(^|\s)'+classes+'\S+/g', ''); });
  },
  endWith : function(obj, class) {
    // TODO
  }
}

var checkPageVisibility = {
  init : function() {
    var hidden, visibilityChange;

    if (typeof document.hidden !== "undefined") {
      hidden = "hidden";
      visibilityChange = "visibilitychange";
    } else if (typeof document.mozHidden !== "undefined") {
      hidden = "mozHidden";
      visibilityChange = "mozvisibilitychange";
    } else if (typeof document.msHidden !== "undefined") {
      hidden = "msHidden";
      visibilityChange = "msvisibilitychange";
    } else if (typeof document.webkitHidden !== "undefined") {
      hidden = "webkitHidden";
      visibilityChange = "webkitvisibilitychange";
    }

    function handleVisibilityChange() {
      if (document[hidden]) {
        pageVisibility.hidden();
      } else {
        pageVisibility.visible();
      }
    }

    // Handle page visibility change
    document.addEventListener(visibilityChange, handleVisibilityChange, false);

  }

}

// Scan through an array of elements on a single line, and apply a class if one of the elementts falls onto a new line.
var lastElementOnLine = {
  init : function(target, element, className) {
    var element = element == null || element == "" ? target + " ul > li" : target + " " + element;
    var className = className == null || className == "" ? "bullet" : className;
    onWindowResize.add("Last element on line", 'lastElementOnLine.action("'+element+'", "'+className+'")');
  },
  action : function(element, className) {
    var lastElement = false;
    $(element).removeClass(className).each(function() {
      if (lastElement && lastElement.offset().top != $(this).offset().top) {
        lastElement.addClass(className);
      }
      lastElement = $(this);
    }).last().addClass(className);
  }
}

// Checks resize break points for min and max browser widths.
// Limits should be parsed as an array of two values. Minumum width first, then Maximum width. e.g. [460, 1080].
// A single number can also be parsed, meaning the 'maxWidth' will be set to what ever the current window width is.
function checkBreakPointLimits(breaks) {
  if (isArray(breaks)) {
    // If "breaks" is an array, use the first two values
    var minWidth = breaks[0] ? breaks[0] : 320;
    var maxWidth = breaks[1] ? breaks[1] : getWindowWidth();
  } else {
    // Otherwise, just use "breaks", and set the maxWidth to the window width
    var minWidth = breaks ? breaks : 320;
    var maxWidth = getWindowWidth();
  }

  if (minWidth <= getWindowWidth() && maxWidth >= getWindowWidth() ) {
    return true;
  }
}

// Get URL hash
function getHash() {
  if(window.location.hash) {
    return window.location.hash.substring(1);
  } else {
    return false;
  }
}

function alignY(obj, offset, equation) {
  var offset = !offset ? 0 : offset;
  $(obj).each(function() {
    calc = ($(this).parent().height()/2) - ($(this).height()/2);
    if (equation && (calc+offset) >= 0) {
      $(this).css({'top':(calc+offset)}); // Offset is added
    } else if ( calc-offset > 0) {
      $(this).css({'top':(calc-offset)}); // Offset is deducted
    }
  });
}

function alignX(obj) {
  $(obj).each(function() {
    if ($(this).parent().width() >= $(this).width()) {
      $(this).css('margin-left', - $(this).width()/2);
    }
  });
}

// Get the browsers scroller width, if there is one.
function getScrollbarWidth() {
  if (document.documentElement.scrollHeight === document.documentElement.clientHeight) {
    return 0;
  } else {
    var $outer = $('<div>').css({visibility: 'hidden', width: 100, overflow: 'scroll'}).appendTo('body');
    var widthWithScroll = $('<div>').css({width: '100%'}).appendTo($outer).outerWidth();
    $outer.remove();
    return 100 - widthWithScroll;
  }
}

// Return window width including scroller bar width
function getWindowWidth() { return $(window).width() + getScrollbarWidth(); }

// Return window height
function getWindowHeight() { return $(window).height(); }

// Limit the amount of characters in a string and return the altered value
function characterLimit(string, limit) {
  return (string.length > limit) ? string.substr(0, limit) + "..." : string;
}

// Add a class to all First and Last elements. Unless there is only one element of it's kind.
function firstAndLast(element) {
  var element = !element ? "li" : element;
  $(element+':first-of-type').each(function(){
    if ($(this).parent().find(element).length > 1) { $(this).addClass("first"); }
  });
  $(element+':last-of-type').each(function(){
    if ($(this).parent().find(element).length > 1) { $(this).addClass("last"); }
  });
}

// Start FastClick
function fastClick() {
  window.addEventListener('load', function() { FastClick.attach(document.body) }, false);
}

// Notice Bars
var noticebars = {
  init: function() {
    $('.noticebar').wrapAll("<div id='noticebars'></div>").each(function() {
      $(this).find('.noticebar-closebtn').click(function(event) {
        event.preventDefault();
        $.cookie($(this).parents('.noticebar').attr('data-cookie'), 'disabled');
        $('#noticebars .noticebar').length != 1 ? $(this).parents('.noticebar').remove() : $('#noticebars').remove();
      });
    });
  }
};

// Change the prefix to all the telephone number links throughout the page to respond differently according to the uers device
function phoneNumbers() {
  $("a[href^='tel'], a[href^='callto']").each(function() {
    if (!isMobile) {
      // Change numbers for VOIP
      $(this).attr('href', $(this).attr('href').replace("tel", "callto"));
    } else {
      // Change numbers for Mobile
      $(this).attr('href', $(this).attr('href').replace("callto", "tel"));
    }
  });
}

// If SVG's are not supported. Run though the img roles throughout the page and add a class to those individual elements
function svgSupport() {
  if(!Modernizr.svg){
    $("*[role='img']").addClass("nosvg");
  }
}

// INIT
$(document).ready(function() {
  firstAndLast();
  phoneNumbers();
  svgSupport();
  connection.init();
  if (isIE){ internetExplorer.init(); }
  if (isMobile) { /*fastClick();*/ onOrientationChange.init(); }
  if ($('.noticebar').length) { noticebars.init(); };
});