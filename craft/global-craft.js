// VARIABLES
var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) );

// Add a notification bar if the user loses internet connection.
var connection = {
  init: function() {
    $( "body" ).attr( {"ononline": "connection.update()", "onoffline": "connection.update()" } );
  },
  status: function() {
    return navigator.onLine;
  },
  update: function() {
    if (navigator.onLine) {
      // Online
      $('body').removeClass('offline');
      $(".noticebar-offline").remove();
    } else {
      // Offline
      $('body').addClass('offline').prepend('<div class="noticebar noticebar-offline"><div class="noticebar-message"><h6>Connection Lost</h6><p>You are currently disconnected from the internet. Please reconnect before you continue</p></div></div>');
      noticebars.init();
    }
  }
};

// INIT
$(document).ready(function() {
  connection.init();
});