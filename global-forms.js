var form = {
  init: function() {

  },
  dropdowns: function() {
    // TODO: If you click on the label to close the dropdown, clicking on it again (without hovering out and back in again)... will not work.
    // TODO: If one dropdown menu expands above another. Rolling off one, only the other... will not close the currently opened dropdown, and not open the newly hovered over dropdown.
    var obj = form,
        label = 'div[role="label"]',
        current = null,
        timer = null,
        isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) );

    // Leave mobile device alone!
    if (!isMobile) {
      $('select').each(function() {
        // Set the scope for 'select'
        var select = $(this);
        // Find all select fields and wrap them in a dropdown 'div' with an empty 'ul'. Also hide them visually.
        select.hide().wrap("<div class='dropdown'></div>").parent().append("<div role='label'><span></span></div><ul></ul>");
        // Now loop through all the 'options' and duplicate the values/text into 'li' elements.
        select.find('option').each(function() {
          // Set the scope for option
          var option = $(this);
          // Find the current 'ul', add an empty 'li', then find that 'li' by querying the last one in the 'ul'.
          // Copy over the text and force everything to lowercase. Then set the value data attribute.
          select.parent().find("ul").append("<li><span></span></li>").children("li").last().attr('data-value', $(this).val()).addClass(function() {
            // Now add some classes depending on what properties were set.
            var props = [];
            if ( option.prop('selected')) { props.push('selected'); }
            if ( option.prop('disabled')) { props.push('disabled'); }
            return props.join(' ');
          }).find('span').text($(this).text().toLowerCase());
        });
        // Set the label text by getting the first 'selected' item.
        select.parent().find(label + " span").text(select.find('option:selected').text());
      });

      $(".dropdown").click(function() {
        current.addClass("hover");
        clearTimeout(timer);
      });

      $(".dropdown").hover(function() {
        current = $(this);
        clearTimeout(timer);
        current.addClass("hover").find("ul li:not(.disabled)").click(function(){
          // Scope for 'li' item
          var item = $(this);
          // Remove all the 'selected' classes
          item.siblings().removeClass('selected');
          // Add a selected class, then fill in the new label text
          item.addClass('selected').parents(".dropdown").find(label + " span").text(item.text());
          // Amend the hidden select dropdown so that the form can read the appropriate data.
          item.parents(".dropdown").find('select option').eq(item.index()).prop('selected', true);
        });
      }, function() {
        timer = setTimeout(function() { current.removeClass( "hover" ) }, 500);
      });

      $(document).click(function(e) {
        if (!$(e.target).hasClass('dropdown') && current != null) {
          current.removeClass("hover");
        }
      });
    } else {
      // For styling, wrap each select in a div. This is mainly because there are a few css attributes set by inherit.
      $('select').each(function() {
        $(this).wrap("<div class='dropdown'></div>");
      });
    }
  }
}